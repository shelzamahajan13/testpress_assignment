from django.conf.urls import url
from django.contrib.auth import views as auth_views
from django.urls import path
from django.conf import settings
from django.conf.urls.static import static
from django.conf.urls.static import static
from .import views

urlpatterns = [
    url(r'^$', views.HomePageOnload, name=''),
	url(r'^reg/$', views.RegistrationPageOnload, name='reg'),
	url(r'^login/$', views.LoginPage, name='login'), 

	url(r'^registeration_user_data/$', views.registeration_user_data, name='update_personnel_data'),
	url(r'^check_login_user_credentials/', views.check_login_user_credentials, name='check_login_user_credentials'),

	url(r'^dashboard/$', views.DashboardPageOnload, name='dashboard'), 
	url(r'^date_time_value_function/(?P<data>[a-zA-Z0-9_#&%@*,():{}" ".|+-]+)$', views.date_time_value_function,name='get_clicked_practice_method'),
	url(r'^get_clicked_practice_quiz/(?P<data>[a-zA-Z0-9_#&%@*,():{}" ".|+-]+)$', views.get_clicked_practice_quiz,name='get_clicked_practice_method'),
    
    
    url(r'^quiz_start/$', views.QuizStartPageOnload, name='quiz_start'), 
    url(r'^check_question_answer/$', views.check_question_answer, name='quiz_start'), 
    url(r'^next_question_answer_set/(?P<data>[a-zA-Z0-9_#&%@*,():{}" ".|+-]+)$', views.next_question_answer_set, name='dashboard'), 
    url(r'^finish_test_report_set_result/(?P<data>[a-zA-Z0-9_#&%@*,():{}" ".|+-]+)$', views.finish_test_report_set_result, name='dashboard'), 
    url(r'^onload_check_question_answer_value/(?P<data>[a-zA-Z0-9_#&%@*,;():{}" ".|+-]+)$', views.onload_check_question_answer_value, name='dashboard'), 
    url(r'^logout/$', views.logout,name='logout'),
    url(r'^practice-report/$', views.PracticeReportPageOnload, name='practice-report'), 
    
    url(r'^openUserRecordsAnswerRecord/(?P<data>[a-zA-Z0-9_#&%@*,():{}" ".|+-]+)$', views.openUserRecordsAnswerRecord, name='dashboard'), 
    url(r'^result_report_status/$', views.result_report_status, name='result_report_status'),
]   
# if settings.DEBUG:
#     urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
