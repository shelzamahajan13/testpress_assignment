from django.apps import AppConfig


class TestpressAppConfig(AppConfig):
    name = 'testpress_app'
