
app.controller("registration_controller",function($scope,$http,CONFIG,fileReader,$rootScope,$timeout,$interval){
	var BASE_URL=JSON.stringify(CONFIG['BASE_URL']).replace(/\"/g, "");
	var FILE_PATH=JSON.stringify(CONFIG['FILE_PATH']).replace(/\"/g, "");
	
	// validation functions
	$scope.validation=function(type,value){
		if((type == "password") && (value!==undefined)){ 
            if(value!=''){
                if(type == "password"){
	                  $scope.passwordid_err= "";
	                  return true;
            	}
            }else{ 
                if(type == "password"){
                  $scope.passwordid_err= "Enter password";
                  return false;
                }
            }
        }else if((type == "password") && (value==undefined)){
                if(type == "password"){
                  $scope.passwordid_err= "Password is required"; 
                  return false;
                }
        } 
        if((type == "conpassword") && (value!==undefined)){ 
                if(value!=''){
                    if(type == "conpassword"){
                      $scope.passwordid1_err= "";
                      return true;
                    }
                }else{ 
                	if(type == "conpassword"){
                  		$scope.passwordid1_err= "Please confirm your password"; 
                  		return false;
                	}
            	}
        }else if((type == "conpassword") && (value==undefined)){
                if(type == "conpassword"){
                  $scope.passwordid1_err= "Confirm password is required"; 
                  return false;
                }
        } 
		if(type == "fname"  && value!== undefined){
				if(value!=''){
					if(type == "fname"){
						$scope.firstnameerr= ""; 
						return true;
					}
				}else{
					$scope.firstnameerr = "Enter first name";
					return false;
				} 
			}else if((type == "fname") && value == undefined){
					$scope.firstnameerr = "First name required";
					return false;
			}
		if(type == "last_name"  && value!== undefined){
			if(value!=''){
				if(type == "last_name"){
					$scope.lname_err= ""; 
					return true;
				}
			}else{
				$scope.lname_err = "Enter last name";
				return false;
			} 
		}else if((type == "last_name") && value == undefined){
				$scope.lname_err = "Last name required";
				return false;
		}
			
			
		if((type == "gender" ) && (value!==undefined)){ 
  			if(value!=''){
	            if(type == "gender"){
	              $scope.gender_err= ""; 
	              return true;
		        }	
    		}else{ 
	            if(type == "gender"){
	                  $scope.gender_err= "Select atleast one option"; 
	                  return false;
	            }
    		}
		}else if((type == "gender" )  && (value==undefined)){
       		if(type == "gender"){
          		$scope.gender_err= "Select atleast one option"; 
	            return false;
          
        	}
        }

    
			
			
		if(type == "date_birth" && value!=undefined){
		
			if(value!==''){
				var valid=$rootScope.check_date(value);
				if(valid==false){
					$scope.date_iderr= "Enter valid date of birth"; 
					return false;
				}else if(valid==true){
					
						$scope.date_iderr= ""; 
						
                		return true;
					}
				
			}else{
				$scope.date_iderr= "Enter date of birth"; 
				return false;
			}
		}else if(type == "date_birth" && value==undefined){
			$scope.date_iderr= "Enter date of birth"; 
			return false;
		}


		if(type == "login_reg_mobile"  && value!== undefined){
			
			if(/^[6789]\d{9}$/.test(value) && value.length ==10){
				if(type == "login_reg_mobile"){
					$scope.email_mobile_err= ""; 
					return true;
				}
			}else{
				$scope.email_mobile_err = "Enter valid mobile number";
				return false;
			} 
		}else if((type == "login_reg_mobile") && value == undefined){
				$scope.email_mobile_err = "Mobile no required";
				return false;
		}

		if(type == "login_reg_email" && value!== undefined) {
			if(/^([a-zA-Z0-9])+([a-zA-Z0-9._%+-])+@([a-zA-Z0-9_.-])+\.(([a-zA-Z]){2,6})$/.test(value)){
				if(type == "login_reg_email"){
					$scope.email_mobile_err = ""; 
					return true;
				}
			}else{
					
					$scope.email_mobile_err = "Enter valid email address";
					return false;}
		}else if((type == "login_reg_email") && value === undefined){
					$scope.email_mobile_err = " Email address required";
					return false;
		}


		if(type == "email_mobile"  && value!== undefined){
			
			if(value!=''){
				if(type == "email_mobile"){
					$scope.email_mobile_err= ""; 
					return true;
				}
			}else{
				$scope.email_mobile_err = "Enter email address or mobile number";
				return false;
			} 
		}else if((type == "email_mobile") && value == undefined){
				$scope.email_mobile_err = "Email address or Mobile number required";
				return false;
		}





		
	}

	$scope.check_enter_value_type=function(email_mobile,bool_value){
		if(bool_value==true){
			if ( !isNaN(email_mobile) && angular.isNumber(+email_mobile)) {
				enter_type='mobile'
				var result=$scope.validation('login_reg_mobile',$scope.user.email_mobile)
				
	    	}else{
	    		enter_type='email'
	    		var result=$scope.validation('login_reg_email',$scope.user.email_mobile)
				
	    	}
	    	var arr= [result, enter_type]
	    	return arr;
		}else{
			result=bool_value
			enter_type=''
			var arr= [result, enter_type]
	    	return arr;
		}
		
	}


	$rootScope.check_date=function(value){
		check_date=value.split('/');
		date=check_date[0];
		month=check_date[1];
		year=check_date[2];


		var dateCheck = /^(0?[1-9]|[12][0-9]|3[01])$/;
		var monthCheck = /^(0[1-9]|1[0-2])$/;
		// var yearCheck = /^\d{4}$/; 
		var yearCheck =/^\b(19|[2-9][0-9])\d{2}$/; 

		if (month.match(monthCheck) && date.match(dateCheck) && year.match(yearCheck)){
			var ListofDays = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
			if (month == 1 || month > 2) {
				if (date > ListofDays[month - 1]) {
					return false;
				}
			}
			if (month == 2) {
				var leapYear = false;
				if ((!(year % 4) && year % 100) || !(year % 400)) {
								leapYear = true;
				}
				if ((leapYear == false) && (date >= 29)) {
								return false;
				}
				if ((leapYear == true) && (date > 29)) {
								return false;
				}

			}
			return true;

		}
		else{
			return false;

		}
	}


	$scope.password_validation=function(){
	    var data1=$scope.user.password;
	    var data=$scope.user.conpassword;
	    if(data=='' && data1==''){
		     return false;
	    }
	    else if(data==data1){
	    	  return true;
	    }else if (data!=data1){
		    $.alert({
			    icon: 'fa fa-warning',
		        title: 'Error!',
		        
		        content: 'Your password do not match.<br>Please try again.',
			    type: 'red',
		        typeAnimated: true,
			});
		
	      	$scope.user.password=''
	      	$scope.user.conpassword=''
	      	$scope.user.agree_term=false;
		 	$("#register_button_id").addClass('disabled')
	      	return false;
	    }
  	}

	// submit user registration data
	$scope.registeration_user_data=function(user){
		var f_name=$scope.validation('fname',$scope.user.fname);
		var l_name=$scope.validation('last_name',$scope.user.last_name);
		var gender=$scope.validation('gender',$scope.user.gender);
		var date_birth=$scope.validation('date_birth',$scope.user.date_birth)
		var password=$scope.validation('password',$scope.user.password);
		var conpassword=$scope.validation('conpassword',$scope.user.conpassword);
		var email_mob_field=$scope.validation('email_mobile',$scope.user.email_mobile);
		if(email_mob_field==true){
			var result_value=$scope.check_enter_value_type($scope.user.email_mobile,true)
		}else{
			var result_value=$scope.check_enter_value_type($scope.user.email_mobile,false)
		}
		var result_status=$scope.password_validation()

		if($scope.euin1_f=='' || $scope.euin1_f==undefined){
			
            var update_profile_image2={'euin1_f':''};
           

            if($scope.update_photo1==undefined){
            	Object.assign(user,euin1_f);

                $scope.euin1_f="";

            }else{
            	var filename = $scope.update_photo1.split('/').pop()
				
				$scope.euin1_f=filename
            }

            
            var update_image_data2=JSON.stringify($scope.euin1_f);
        }else{
        	
                var file=$scope.euin1_f;
                var filename=JSON.stringify(file['filename']);
                var euin1_f={'euin1_f':filename};
                Object.assign(user,euin1_f);
                if($scope.euin1_f!='' || $scope.euin1_f!=undefined){
                    var update_image_data2=JSON.stringify($scope.euin1_f);
                }
        }

       

		
		var data=angular.copy(user)
		var data=JSON.stringify(data)
		if(result_value[0]==true && result_status==true && f_name==true && date_birth==true &&  l_name==true  && gender==true  && password==true && conpassword==true){
			$(".loader").show()
			$.ajax({
				url:'/registeration_user_data/',
				type:'POST',
				dataType: 'json',
				data:{'data':data,'update_image_data2':update_image_data2},
				success: function(response){
					$(".loader").hide()
					var a=response['status'];
					
					if(a=="success"){
						tick_path=BASE_URL+"static/img/general-images/tick.png"
				        $.confirm({
				        	
				            theme: 'modern',
				            columnClass: 'medium',
				            title: 'Congratulations!!',
				            content: 'Your account has been registered successfully!<br>',
				            type: 'green',
				            animation: 'RotateYR',
				            closeAnimation: 'RotateYR',
				            typeAnimated: true,
				            buttons: {
				              Cancel: function () {
				                location.reload()
				              },
				              Login: function () {
				                location.href = BASE_URL+'login/'
				              },

				            }
				        });

				        $
			  		 	$(".empty_value").val('')
			  		 	
					}
					
					if(a=='exists'){
						var enter_value=result_value[1]
						if(enter_value=='mobile'){
							content='Mobile number'
						}else{
							content='Email id'
						}
						$.alert({
				            icon: 'fa fa-exclamation-circle',
				            theme: 'white',
				            
				            animation: 'scale',
				            type: 'red',
				            title: 'Alert!',
				            content: ''+content+' already exist.',
				        })
					}
				},function (response) {
					if(response.status=="500"){
						console.log('wrong1'); 
					}
				}
			})
		}
	}

	$(".thumbnail").hide()
	$scope.getFile18 = function () {
	    $scope.progress = 0;

        fileReader.readAsDataUrl($scope.file, $scope)
                      .then(function(result) {
                      	  $(".loader").hide()
                      	  $(".thumbnail").show()
                          $scope.update_photo1 = result;
                          $scope.imageerr1 =''
                          
                      });
    };

    $scope.inputType='password';

    $scope.show_password=function(){
		if($scope.user.password!=null){
		
			if($scope.inputType=='password'){
					$scope.inputType='text'
					$scope.user.password_value=true
			}else{
					$scope.inputType='password';
					$scope.user.password_value=false
			}
		}
	}

    $scope.check_login_user_credentials=function(user){
    	
    	var mobile=$scope.validation('email_mobile',$scope.user.email_mobile)
		var password=$scope.validation('password',$scope.user.password)
		
		if(mobile==true && password==true){
			var result_value=$scope.check_enter_value_type($scope.user.email_mobile,true)
			
			if(result_value[0]==true){
				var data=angular.copy({"email_mobile_login":$scope.user.email_mobile,"password":$scope.user.password})
				var data=JSON.stringify(data)

				if(result_value[1]=='mobile'){
					content='Mobile number'
				}else{
					content='Email id'
				}
				$(".loader").show()
				$.ajax({
					url:'/check_login_user_credentials/',
					type:'POST',
					dataType: 'json',
					data:{'data':data},

					success: function(response){
						$(".loader").hide()
						var a=response['status']
						dy_user_id = response['user_id'];
						
						if(a=='success'){
							
							$('.login_success').html('<p class="alert alert-success" style="color: black;border-radius:15px"><strong>Success!</strong>Login Successfully.</p>');
							$('.username_err').html('');
							window.setTimeout(function(){
		                        location = BASE_URL+"dashboard/"
		                     },0)
							
						}else if(a=='wrong'){
							$('.username_err').html('<p class="alert alert-danger error_color" style="border-radius:15px">Please enter valid password.</p>');
							$('.login_success').html('')
							$("#password").val('')
						}else if(a=='email_mobile'){
							$('.username_err').html('<p class="alert alert-danger error_color" style="border-radius:15px">'+content+' not exist.</p>');
							$('.login_success').html('')
							$("#password").val('')
							$("#email_mob_login").val('')
						}

						
						
					},function (response) {
						if(response.status=="500"){
							console.log('wrong1'); 
						}
					}


				})
			}
			
		}
    }

    
	


});