var check_question_answer_result;
var next_question_answer_set_result;
var finish_test_report_set_result;
var dashboard_redirect_value;

function check_question_answer(){
	
	check_question_answer_result()
}

function next_question_answer_set(){
	next_question_answer_set_result()
}

function finish_test_report_set(){
	finish_test_report_set_result()
}

function wb_openNav() {
    
    document.getElementById("wb_myNav").style.height = "100%";
   
}

function dashboard_redirect(){
	
	dashboard_redirect_value()
}

function wb_closeNav() {
    
    document.getElementById("wb_myNav").style.height = "0%";
    $('.wb_overlay-content').empty();
    $('.wb_overlay-content').css("display", "block");

}

function openNav() {
 $('#mySidebar').show();
 $('.overlay_sec').addClass('animated slideInRight');
 setTimeout(function(){ $('.overlay_sec').removeClass('animated slideInRight'); }, 1000);

 }
 function closeNav() {
 
 $('.overlay_sec').addClass('animated slideOutRight');
  setTimeout(function(){  
  $('#mySidebar').hide();
  $('.overlay_sec').removeClass('animated slideOutRight');
  }, 1000);
}




app.controller("dashboard_data_controller",function($scope,$http,CONFIG,fileReader,$rootScope,$timeout,$interval){
	var BASE_URL=JSON.stringify(CONFIG['BASE_URL']).replace(/\"/g, "");



	var tick = function() {
		var current_date=new Date();
	    $scope.clock =current_date;
	    var get_hours=current_date.getHours();
	    var TimeOut_Thread = $timeout(function(){ $scope.date_time_value_function(get_hours) } , 100);
	}
	tick();
	$interval(tick, 500);

	$scope.date_time_value_function=function(get_hours){
		
		if($scope.user.dy_user_id!=''){
    		var data=JSON.stringify({"get_hours":get_hours})
    		
    		$http.post(BASE_URL+"date_time_value_function/"+data).then(function(response){
    			$scope.greeting_name=JSON.stringify(response.data.greeting_name).replace(/\"/g, "")
    			
    		},function(response){
    			console.log("error")
    		})
    		
    	}
	}

	$scope.get_clicked_practice_quiz=function(category_id,category_name){
		var data=JSON.stringify({"category_id":category_id,"category_name":category_name})
		$(".loader").show()
		$http.post(BASE_URL+"get_clicked_practice_quiz/"+data).then(function(response){
			$(".loader").hide()
			var get_check_status=JSON.stringify(response.data.a_res).replace(/\"/g, "")
			if(get_check_status=='0'){
				$.alert({
		            icon: 'fa fa-exclamation-circle',
		            theme: 'white',
		            animation: 'scale',
		            type: 'red',
		            title: 'Error!',
		            content: 'Error in generating API.',
		        })

				
			}else{
				window.location.href=BASE_URL+"quiz_start/"
			}
		},function(response){
			console.log("error")
		})

	}

	$scope.onload_check_question_answer_value=function(user_id,current_question){
		var data=JSON.stringify({"current_question":current_question,'user_id':user_id})
		$http.post(BASE_URL+"onload_check_question_answer_value/"+data).then(function(response){
			$("#questions_tab").empty()
			var content_new=response.data.content
			$("#questions_tab").html(content_new)
		},function(response){
			console.log("error")
		})
	}

	$scope.check_question_answer_result=function(){
		
		answer_value=$('input[name="aa"]:checked').val()

		if(answer_value!=undefined){
			var data=JSON.stringify({"answer_value":answer_value})

			$.ajax({
				url:'/check_question_answer/',
				type:'POST',
				dataType: 'json',
				data:{'data':data},
				success: function(response){
					$(".loader").hide()
					var get_result=response['a_res'];
					
					if(get_result=='0'){
						$(".result_display").html('<span class="mt-2 text-success" style="font-size:14px"><i class="fa fa-check-circle" style="font-size:14px"  aria-hidden="true"></i> Correct Answer.</span>');
					}else{
						var get_answer=JSON.stringify(response['get_correct_answer']).replace(/\"/g, "")
						$(".result_display").html('<span class="mt-2 text-danger" style="font-size:14px"><i class="fa fa-times-circle" style="font-size:14px"  aria-hidden="true"></i> Wrong Answer.<p class="text-success mt-2 ">Correct Anwser: '+get_answer+'</p></span>');
					}
					$(".submit_button").removeClass("disabled")

					var inputs = document.querySelectorAll('input[type="radio"]');
					for (var i = 0; i < inputs.length; i++) {
					  inputs[i].disabled = 'true';
					}

				},function (response) {
					if(response.status=="500"){
						console.log('wrong1'); 
					}
				}
			})

		}else{
			alert("Please select atleast one option")
		}
	}

	check_question_answer_result=$scope.check_question_answer_result


	$scope.next_question_answer_set_result=function(){
		var data=JSON.stringify({"user_id":$scope.user.user_id})
		$(".loader").show()
		$http.post(BASE_URL+"next_question_answer_set/"+data).then(function(response){
			$(".loader").hide()
			var current_question=JSON.stringify(response.data.cuurent_question).replace(/\"/g, "")
			$scope.onload_check_question_answer_value($scope.user.user_id,current_question)
		
		},function(response){
			console.log("error")
		})
	}
	next_question_answer_set_result=$scope.next_question_answer_set_result

	$scope.finish_test_report_set_result=function(){
		var data=JSON.stringify({"user_id":$scope.user.user_id})
		$(".loader").show()
		$http.post(BASE_URL+"finish_test_report_set_result/"+data).then(function(response){
			wb_openNav();
			$('.closebtn').hide();
			file_name_path=BASE_URL+'result_report_status/'
			$(".wb_overlay-content").load(file_name_path);
		},function(response){
			console.log("Error")
		})
	}
	finish_test_report_set_result=$scope.finish_test_report_set_result

	$scope.dashboard_redirect_value=function(){
		
		window.location.href=BASE_URL+"dashboard/"
	}
	dashboard_redirect_value=$scope.dashboard_redirect_value

	$(document).ready( function () {
	    var table_data=$('#datatablesRecord').DataTable();

	    $('#data_table_search_items').keyup( function() {
	      
	      	table_data.search( this.value ).draw();
	    });
	} );

	$scope.openUserRecordsAnswerRecord=function(table_id){

		wb_openNav();
		$('.closebtn').show();
		file_name_path=BASE_URL+'openUserRecordsAnswerRecord/'+table_id
		$(".wb_overlay-content").load(file_name_path);
		
	}
	
})