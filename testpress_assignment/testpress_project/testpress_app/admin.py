from django.contrib import admin
from django.contrib import admin

from .models import UserRegisterationDetails,UserSavedDatabaseAnswer

class UserRegisterationDetailsAdmin(admin.ModelAdmin):
	list_display = ('id','auth_user','first_name', 'last_name','email_mobile', 'gender','dob','photo','password','user_registered_date')

	search_fields = ('id','auth_user','first_name', 'last_name','email_mobile', 'gender','dob','photo','password','user_registered_date')


	row_id_fields=('auth_user',)
	
	def User(self, obj):
		return obj.auth_user.user

class UserSavedDatabaseAnswerAdmin(admin.ModelAdmin):
	list_display = ('id','register_user_id','quiz_category_name', 'question_name','user_answers', 'result','corrected_answer','cuurent_practice_timestamp')

	search_fields = ('id','register_user_id','quiz_category_name', 'question_name','user_answers', 'result','corrected_answer','cuurent_practice_timestamp')


	row_id_fields=('register_user_id',)
	
	def User(self, obj):
		return obj.register_user_id.id


	
admin.site.register(UserRegisterationDetails,UserRegisterationDetailsAdmin)
admin.site.register(UserSavedDatabaseAnswer,UserSavedDatabaseAnswerAdmin)
