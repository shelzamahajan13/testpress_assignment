from django.db import models
from django.utils import timezone
from django.http import HttpResponse
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver


class UserRegisterationDetails(models.Model):
	auth_user = models.ForeignKey(User, on_delete=models.CASCADE)
	first_name=models.CharField(max_length=200,null=True,blank=True)
	last_name=models.CharField(max_length=50,null=True,blank=True)
	email_mobile=models.CharField(max_length=100, null=True,blank=True)
	gender = models.CharField(max_length=100,null=True,blank=True)
	dob = models.DateField(null=True,blank=True)
	photo = models.CharField(max_length=100,null=True,blank=True)
	password=models.CharField(max_length=100,null=True,blank=True)
	user_registered_date=models.DateTimeField('user_registered_date',null=True,blank=True)

class UserSavedDatabaseAnswer(models.Model):
	register_user_id=models.ForeignKey(UserRegisterationDetails, on_delete=models.CASCADE)
	quiz_category_name=models.CharField(max_length=100,null=True,blank=True)
	question_name=models.CharField(max_length=5000,null=True,blank=True)
	user_answers=models.CharField(max_length=3000,null=True,blank=True)
	result=models.CharField(max_length=100,null=True,blank=True)
	corrected_answer=models.CharField(max_length=2000,null=True,blank=True)
	cuurent_practice_timestamp=models.DateTimeField('user_registered_date',null=True,blank=True)