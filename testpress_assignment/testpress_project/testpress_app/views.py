from django.shortcuts import render,redirect
from django.http import HttpResponse
from django.views.generic import TemplateView,ListView
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth.models import User

import logging
import requests
import json
import string,re
from django.contrib.auth import authenticate, login

import io
import sys
from django.utils import timezone
from datetime import datetime,timedelta
import datetime
from .models import UserRegisterationDetails,UserSavedDatabaseAnswer
from .static_file_path import *
import os

import base64
import urllib.request
import urllib.parse

from django import template
from django.template.loader import get_template



stdlogger = logging.getLogger(__name__)

log = logging.getLogger(__name__)

if __name__ == '__main__':
	sys.exit(main())

# Create your views here.
def request_session_check(request):
	if 'user_id' in request.session:
		user_id = request.session['user_id']
		log.info(user_id)
		get_firstname=UserRegisterationDetails.objects.get(id=user_id).first_name
		log.info(get_firstname)
		get_lastname=UserRegisterationDetails.objects.get(id=user_id).last_name
		log.info(get_lastname)

		get_server_current_time= timezone.now()
		
		get_current_timestamp= get_server_current_time +datetime.timedelta(hours=5,minutes=30)

		get_format_current_date=str(get_current_timestamp)[:19]
				
		get_current_timestamp_new = datetime.datetime.strptime(get_format_current_date, '%Y-%m-%d %H:%M:%S')
		get_current_date=get_current_timestamp_new.date()

		get_month=get_current_date.strftime('%B') 
		day=get_current_date.day
		day_double=str(day).zfill(2)
		year=get_current_date.year
		get_current_date_value=str(day_double)+' '+str(get_month)+' '+str(year)
	
	else:
		user_id=''
		get_firstname=''
		get_lastname=''

	return get_firstname+'|'+get_lastname+'|'+str(user_id)+'|'+get_current_date_value

def RegistrationPageOnload(request):
	get_key = request.session.has_key('user_id')
	
	
	if get_key!=False:
		try:
			data=request_session_check(request);
			data=data.split('|');

			return render(request,'registration.html',{

				'get_firstname':data[0].title(),
				'get_lastname':data[1].title(),
				'user_id':data[2],
				
			})

		except Exception as ex:
			log.error("Error in redirecting page: %s" % ex)
	else:
		user_id=''
		return render(request,'registration.html',{"user_id":user_id})

def HomePageOnload(request):
	get_key = request.session.has_key('user_id')
	
	
	if get_key!=False:
		try:
			data=request_session_check(request);
			data=data.split('|');

			return render(request,'home.html',{

				'get_firstname':data[0].title(),
				'get_lastname':data[1].title(),
				'user_id':data[2],
				
			})

		except Exception as ex:
			log.error("Error in redirecting page: %s" % ex)
	else:
		user_id=''
		return render(request,'home.html',{"user_id":user_id})


def LoginPage(request):

	get_key = request.session.has_key('user_id')
	
	if get_key!=False:
		try:
			data=request_session_check(request);
			data=data.split('|');

			return render(request,'login.html',{

				'get_firstname':data[0].title(),
				'get_lastname':data[1].title(),
				'user_id':data[2],
				
			})

		except Exception as ex:
			log.error("Error in redirecting page: %s" % ex)
	else:
		user_id=''
		return render(request,'login.html',{"user_id":user_id})


@csrf_exempt
def registeration_user_data(request):
	result={}
	try:
		req_data=request.POST.get('data')
		req_data=json.loads(req_data)
		
	except Exception as ex:
		log.error("Error in redirecting page: %s" % ex)
	try:
		if request.method=="POST":
				
				dob=req_data['date_birth']
				
				
				if dob!='':
					d,m,y=dob.split("/")
					user_dob_format=y+'-'+m+'-'+d
					
				elif dob =='':
					user_dob_format=None
					

				cureent_register_timestamp= timezone.now()
				
				cureent_register_timestamp_new= cureent_register_timestamp +datetime.timedelta(hours=5,minutes=30)
				
				
				if not(User.objects.filter(username=req_data['email_mobile'])).exists():
					
					User.objects.create_user(req_data['email_mobile'],req_data['email_mobile'],req_data['password'])
					atuser = User.objects.get(username=req_data['email_mobile'])
					

					UserRegisterationDetails.objects.create(auth_user=atuser,
						first_name=req_data['fname'].strip(),
						last_name=req_data['last_name'].strip(),
						gender=req_data['gender'].strip(),
						dob=user_dob_format.strip(),
						email_mobile=req_data['email_mobile'].strip(),
						password=req_data['password'].strip(),
						user_registered_date=cureent_register_timestamp_new,
						).save()

					get_user_register_instance=UserRegisterationDetails.objects.get(email_mobile=req_data['email_mobile']).id

					profile_photo=request.POST.get('update_image_data2')
					log.info(profile_photo)
					
					profile_photo_data=json.loads(profile_photo)
					if profile_photo_data:
							log.info("ggg")
							
							filenamess=profile_photo_data['filename']
							filetypess=profile_photo_data['filetype']
							
							base641ss=profile_photo_data['base64']
							if os.path.exists(path1+'Users_Documents/'):
								print("exists")
								(status,outfile)=upload_profile_photo(get_user_register_instance,filetypess,base641ss)
								print(status)
								print(outfile)
							else:
								createFolder(path1+'Users_Documents/')
								(status,outfile)=upload_profile_photo(get_user_register_instance,filetypess,base641ss)
								print(status)
								print(outfile)

							if status==True:
								outfile=outfile.split("/")
								print(outfile)
								outfile=outfile[5] 	
								print(outfile)
							
							update_profile_path=UserRegisterationDetails.objects.get(id=get_user_register_instance)
								
							update_profile_path.photo=outfile
							update_profile_path.save()



					else:
						outfile=''
						log.info("same")

					

				
					result['status']='success'
				else:
					
					result['status']='exists'
					
		return HttpResponse(json.dumps(result), content_type='application/json')
	except Exception as ex:
		log.error("Error in redirecting page: %s" % ex)



def createFolder(directory):
	try:
		if not os.path.exists(directory):
			os.makedirs(directory)
	except OSError:
		print ('Error: Creating directory. ' +  directory)

def upload_profile_photo(user_id,filetypess,base641ss):
		try:
			
			img_name='User'+str(user_id)+'_photo'
			print(img_name)
			
			
			a,ext=filetypess.split("/")

			infile = path1+'Users_Documents/'+img_name+"."+ext
			
			img_decoded = base64.b64decode(base641ss)
			
			with open(infile, "wb") as fh:
					fh.write(img_decoded)

			f, e = os.path.splitext(infile)
			outfile = f + ".png"
			if infile != outfile and (filetypess!='png' or filetypess!='PNG'):
					try:
						Image.open(infile).save(outfile, "png")
						os.unlink(infile)
						
					except IOError:
						log.info("cannot convert")
			else:
					log.info('Already Exists')

			return (True,outfile)

		except Exception as ex:
			log.error('Error in uploading file: %s' % ex)
			return False;

@csrf_exempt
def check_login_user_credentials(request):
	login_result={}
	try:
		req_data=request.POST.get('data')
		req_data=json.loads(req_data)
		
	except Exception as ex:
		log.error("Error in redirecting page: %s" % ex)
	try:
		if request.method=="POST":
			
			if request.method=="POST":
				if User.objects.filter(username=req_data['email_mobile_login']).exists():
					user= authenticate(username=req_data['email_mobile_login'], password=req_data['password'])
					
					if(user!=None):
						login(request, user)
						current_user = request.user
						

						get_user_id = User.objects.get(username=req_data['email_mobile_login']).id
						

						if UserRegisterationDetails.objects.filter(auth_user=get_user_id).exists():
							
							user_id=UserRegisterationDetails.objects.get(auth_user=get_user_id).id
							
							
							if 'user_id' in request.session:
								request.session['user_id'] = user_id
							else:
								request.session['user_id'] = user_id

							
							login_result['user_id']=user_id
							
							login_result['status']='success'

						else:
						
							login_result['status']="wrong"
							
					else:
						
						login_result['status']="wrong"
				else:
					login_result['status']="email_mobile"
			return HttpResponse(json.dumps(login_result), content_type='application/json')
		return HttpResponse(json.dumps(login_result), content_type='application/json')
	except Exception as ex:
					log.error('Error in json data: %s' % ex) 

@csrf_exempt
def DashboardPageOnload(request):
	try:
		
		get_key = request.session.has_key('user_id')
		
		if get_key!=False:
			data_get=request_session_check(request)
			data_values=data_get.split("|")

			
			response_api=requests.get("https://opentdb.com/api_category.php")
				
			result_set_api=response_api.json()

			json_category_data=result_set_api['trivia_categories']

			return render(request,'dashboard.html',{
				
				"get_firstname":data_values[0].title(),
				"get_lastname":data_values[1].title(),
				"current_date":data_values[3],
				
				"user_id":data_values[2],
				"result_set_json":json_category_data
				
				})
		else:
			 return redirect('http://'+request.get_host()+'/')
	except Exception as ex:
			log.error('Error in json data: %s' % ex)

@csrf_exempt
def date_time_value_function(request,data):
	result={}
	try:
		req_data=json.loads(data)
		
	except Exception as ex:
			log.error('Error in json data: %s' % ex)
	try:
		if request.method=='POST':
			
			if req_data['get_hours'] >= 4 and req_data['get_hours']<12:
				greeting_name    = "Good Morning"; 
			elif req_data['get_hours'] >= 12 and req_data['get_hours']<16:
				greeting_name   = "Good Afternoon";
			elif req_data['get_hours'] >= 16 and req_data['get_hours']<20: 
				greeting_name    = "Good Evening";
			elif req_data['get_hours'] >= 20 and req_data['get_hours']<4:
				greeting_name    = "Peaceful Night";
			else:
			   greeting_name="Be Relax - Be Happy"; 

			
			
			result['greeting_name']=greeting_name
		return HttpResponse(json.dumps(result), content_type='application/json')
	except Exception as ex:
		log.error("Error in redirecting page: %s" % ex)

@csrf_exempt
def get_clicked_practice_quiz(request,data):
	result={}
	try:
		req_data=json.loads(data)
		
	except Exception as ex:
			log.error('Error in json data: %s' % ex)
	try:
		if request.method=='POST':

			response_quiz_api=requests.get("https://opentdb.com/api.php?amount=10&category="+req_data['category_id']+"&difficulty=medium&type=multiple")				
				
			result_quiz_set_api=response_quiz_api.json()

			get_Response_code=result_quiz_set_api['response_code']
			log.info(get_Response_code)

			quiz_set_question_api=result_quiz_set_api['results']
			log.info(quiz_set_question_api)
			
			
			if get_Response_code==0:
			
				quiz_set_question_api=result_quiz_set_api['results']
				log.info(quiz_set_question_api)
				if 'category_quiz_name' in request.session:
					if request.session['category_quiz_name']!= req_data['category_name']:
						del request.session['category_quiz_name']
						request.session['category_quiz_name']=req_data['category_name']

						del request.session['question_set_api']
						request.session['question_set_api']=quiz_set_question_api

						request.session['session_practice_count_value']='0'
					else:

						request.session['category_quiz_name']=req_data['category_name']
						quiz_set_question_api=request.session['question_set_api']
						get_session_practice_count_value=request.session['session_practice_count_value']
				else:
					request.session['category_quiz_name']=req_data['category_name']
					if 'question_set_api' in request.session:
						del request.session['question_set_api']
						request.session['question_set_api']=quiz_set_question_api
					else:
						request.session['question_set_api']=quiz_set_question_api

					request.session['session_practice_count_value']='0'

				result['a_res']='1'
			else:
				result['a_res']='0'

			

		return HttpResponse(json.dumps(result), content_type='application/json')
	except Exception as ex:
		log.error("Error in redirecting page: %s" % ex)

@csrf_exempt
def QuizStartPageOnload(request):
	try:
		
		get_key = request.session.has_key('user_id')
		
		if get_key!=False:
			data_get=request_session_check(request)
			data_values=data_get.split("|")

			log.info(request.session['question_set_api'])

			get_server_current_time= timezone.now()
			
			get_current_timestamp= get_server_current_time +datetime.timedelta(hours=5,minutes=30)

			if 'session_practice_count_value' in request.session:
				get_variable_value=request.session['session_practice_count_value']
			else:
				request.session['session_practice_count_value']=0

			log.info(request.session['session_practice_count_value'])

			if request.session['session_practice_count_value']=='0':
				if 'session_current_quiz_question' in request.session:
					
					del request.session['session_current_quiz_question']
					request.session['session_current_quiz_question']=0
				else:
					
					request.session['session_current_quiz_question']=0
				log.info(request.session['session_current_quiz_question'])

				if 'session_quiz_start_timestamp' in request.session:
					del request.session['session_quiz_start_timestamp']
					request.session['session_quiz_start_timestamp']=str(get_current_timestamp)
				else:
					request.session['session_quiz_start_timestamp']=str(get_current_timestamp)

			else:
				if 'session_quiz_start_timestamp' in request.session:
					get_timestamp=request.session['session_quiz_start_timestamp']

				if 'session_current_quiz_question' in request.session:
					
					get_curerent_value=request.session['session_current_quiz_question']
				else:
					
					request.session['session_current_quiz_question']=0

			log.info(request.session['session_current_quiz_question'])

			return render(request,'quiz.html',{
				
				"get_firstname":data_values[0].title(),
				"get_lastname":data_values[1].title(),
				"current_date":data_values[3],
				"user_id":data_values[2],
				
				
				})
		else:
			 return redirect('http://'+request.get_host()+'/')
	except Exception as ex:
			log.error('Error in json data: %s' % ex)


@csrf_exempt
def onload_check_question_answer_value(request,data):
	result={}
	try:
		
		req_data=json.loads(req_data)
		
	except Exception as ex:
			log.error('Error in json data: %s' % ex)
	try:
		if request.method=='POST':
			
			total_questions=10

			if int(request.session['session_current_quiz_question'])==int(total_questions-1):
				button_value='Finish'
			else:
				button_value='Submit'


			get_current_question_value_set=request.session['question_set_api'][int(request.session['session_current_quiz_question'])]
			
			json_value = {
				'get_current_question_value_set': get_current_question_value_set,
				'button_value':button_value
			}
			template = get_template("quiz_question_answer.html")

			content = template.render(json_value)
			
			
			result['content']=content
		return HttpResponse(json.dumps(result), content_type='application/json')
	except Exception as ex:
		log.error("Error in redirecting page: %s" % ex)




@csrf_exempt
def check_question_answer(request):
	result={}
	try:
		req_data=request.POST.get('data')
		req_data=json.loads(req_data)
		
	except Exception as ex:
			log.error('Error in json data: %s' % ex)
	try:
		if request.method=='POST':
			
			get_correct_answer=request.session['question_set_api'][int(request.session['session_current_quiz_question'])]['correct_answer']
			
			session_user_set=[]
			session_result_set=[]
			if get_correct_answer==req_data['answer_value']:
				result_val='T'
				result['a_res']='0'
			else:
				result_val='F'
				result['get_correct_answer']=get_correct_answer
				result['a_res']='1'



			if 'session_user_answer' in request.session:
				
				if (int(request.session['session_current_quiz_question'])+int(1))==len(request.session['session_user_answer']):
					
					del request.session['session_user_answer'][-1]
		
					request.session['session_user_answer'].append(req_data['answer_value'])
					request.session['session_user_answer']=request.session['session_user_answer']
				else:
					
					request.session['session_user_answer'].append(req_data['answer_value'])
					request.session['session_user_answer']=request.session['session_user_answer']
			else:
			
				session_user_set.append(req_data['answer_value'])
				request.session['session_user_answer']=session_user_set
			
			log.info(request.session['session_user_answer'])

			if 'session_result_answer' in request.session:
				if (int(request.session['session_current_quiz_question'])+int(1))==len(request.session['session_result_answer']):
					del request.session['session_result_answer'][-1]
					request.session['session_result_answer'].append(result_val)
					request.session['session_result_answer']=request.session['session_result_answer']
				else:
					request.session['session_result_answer'].append(result_val)
					request.session['session_result_answer']=request.session['session_result_answer']
			else:
				session_result_set.append(result_val)
				request.session['session_result_answer']=session_result_set

			log.info(request.session['session_result_answer'])




		return HttpResponse(json.dumps(result), content_type='application/json')
	except Exception as ex:
		log.error("Error in redirecting page: %s" % ex)


@csrf_exempt
def next_question_answer_set(request,data):
	result={}
	try:
		req_data=json.loads(data)
		
	except Exception as ex:
			log.error('Error in json data: %s' % ex)
	try:
		if request.method=='POST':
			
			question_set_full_result=[]
			correct_answer_full_set=[]
			
			for i in request.session['question_set_api']:
				question_set=i['question']
				correct_answer=i['correct_answer']
				result_set={'question_set':question_set}
				correct_set={'correct_answer':correct_answer}
				question_set_full_result.append(question_set)
				correct_answer_full_set.append(correct_answer)

			log.info(question_set_full_result)
			
			if not UserSavedDatabaseAnswer.objects.filter(register_user_id=request.session['user_id'],quiz_category_name=request.session['category_quiz_name'],cuurent_practice_timestamp=request.session['session_quiz_start_timestamp']).exists():

				get_register_instance=UserRegisterationDetails.objects.get(id=request.session['user_id'])

				UserSavedDatabaseAnswer.objects.create(
					register_user_id=get_register_instance,
					quiz_category_name=request.session['category_quiz_name'],
					cuurent_practice_timestamp=request.session['session_quiz_start_timestamp'],
					user_answers=request.session['session_user_answer'],
					result=request.session['session_result_answer'],
					question_name=question_set_full_result,
					corrected_answer=correct_answer_full_set
				).save()
			else:
				get_update_inmstance=UserSavedDatabaseAnswer.objects.get(register_user_id=request.session['user_id'],quiz_category_name=request.session['category_quiz_name'],cuurent_practice_timestamp=request.session['session_quiz_start_timestamp'])
				get_update_inmstance.user_answers=request.session['session_user_answer']
				get_update_inmstance.result=request.session['session_result_answer']
				get_update_inmstance.save()
			
			request.session['session_current_quiz_question']=int(request.session['session_current_quiz_question'])+int(1)
			
			request.session['session_practice_count_value']='1'
			result['cuurent_question']=request.session['session_current_quiz_question']
		return HttpResponse(json.dumps(result), content_type='application/json')
	except Exception as ex:
		log.error("Error in redirecting page: %s" % ex)


@csrf_exempt
def finish_test_report_set_result(request,data):
	result={}
	try:
		req_data=json.loads(data)
		
	except Exception as ex:
			log.error('Error in json data: %s' % ex)
	try:
		if request.method=='POST':
			get_update_inmstance=UserSavedDatabaseAnswer.objects.get(register_user_id=request.session['user_id'],quiz_category_name=request.session['category_quiz_name'],cuurent_practice_timestamp=request.session['session_quiz_start_timestamp'])
			get_update_inmstance.user_answers=request.session['session_user_answer']
			get_update_inmstance.result=request.session['session_result_answer']
			get_update_inmstance.save()

			get_server_current_time= timezone.now()
			
			get_end_current_timestamp= get_server_current_time +datetime.timedelta(hours=5,minutes=30)

			get_end_timestamp_format= get_end_current_timestamp.strftime('%Y-%m-%d %H:%M:%S.%f')
				
			end_practice_time_format = get_end_timestamp_format[:19]
			datetimeFormat = '%Y-%m-%d %H:%M:%S' 
			end_practice_time = datetime.datetime.strptime(end_practice_time_format,datetimeFormat)


			start_practice_time=request.session['session_quiz_start_timestamp']
				
			start_practice_time_format = start_practice_time[:19]
			
			
			exact_start_practice_time = datetime.datetime.strptime(start_practice_time_format,datetimeFormat)
			
			
			time_difference=end_practice_time - exact_start_practice_time
			
			time_difference_seconds=time_difference.seconds
		
			get_practice_duration=int((time_difference_seconds)/60)

			if int(get_practice_duration) == int('0'):
				
				practice_duration_status_value='1 min'

			elif int(get_practice_duration) < int('60'):
				
				
				practice_duration_status_value=str(get_practice_duration)+" min"

			elif int(get_practice_duration) > int('60'):
				
				get_hours_values=int(get_practice_duration)/60
				get_exact_hours_value=round(get_hours_values)
				get_minutes_value=get_exact_hours_value*60
				get_minutes_value_exact=int(get_practice_duration)-int(get_minutes_value)
				practice_duration_status_value=str(get_exact_hours_value)+" hr "+str(get_minutes_value_exact)+" min" 
				
			elif int(get_practice_duration) == int('60'):
				
				practice_duration_status_value='1 hr'
			

			request.session['practice_time_duration']=practice_duration_status_value

			count_attempted=0
			count_wrong=0

			for i in request.session['session_result_answer']:
				
				if i=='T':
					count_attempted=count_attempted+1

				elif i=='F':
					count_wrong=count_wrong+1

			request.session['session_correct_answer_number']=count_attempted
			# request.session['session_practice_count_value']='0'
					
		return HttpResponse(json.dumps(result), content_type='application/json')
	except Exception as ex:
		log.error("Error in redirecting page: %s" % ex)

@csrf_exempt
def result_report_status(request):
	try:
		
		get_key = request.session.has_key('user_id')
		
		if get_key!=False:
			get_date_time_value=request.session['session_quiz_start_timestamp']
			get_format_current_date=str(get_date_time_value)[:19]
	
			get_current_timestamp_new = datetime.datetime.strptime(get_format_current_date, '%Y-%m-%d %H:%M:%S')
			get_current_date=get_current_timestamp_new.date()

			get_month=get_current_date.strftime('%b') 
			day=get_current_date.day
			day_double=str(day).zfill(2)
			year=get_current_date.year
			get_current_date_value=str(day_double)+'/'+str(get_month)+'/'+str(year)

			get_current_time=get_current_timestamp_new.time()

			get_current_time_value=get_current_time.strftime("%I:%M:%S %p")

			if request.session['session_correct_answer_number']<4:
				result_status='Fail'
			else:
				result_status='Pass'

			return render(request,'feedback_report_result.html',{
				
				"get_current_time_value":get_current_time_value,
				"get_current_date_value":get_current_date_value,
				"result_status":result_status
				
				})
		else:
			 return redirect('http://'+request.get_host()+'/')
	except Exception as ex:
			log.error('Error in json data: %s' % ex)

@csrf_exempt
def logout(request):
	
	try: 
		
		get_session_key=request.session.session_key
		request.session.flush()

		return redirect('http://'+request.get_host()+'/login')

	except Exception as ex: 
		log.error('Error in Logout API: req_data: %s' % ex)


@csrf_exempt
def PracticeReportPageOnload(request):
	try:
		get_key = request.session.has_key('user_id')
		
		if get_key!=False:
			if UserSavedDatabaseAnswer.objects.filter(register_user_id=request.session['user_id']).exists():
				get_information_data=UserSavedDatabaseAnswer.objects.filter(register_user_id=request.session['user_id']).values('id','quiz_category_name','cuurent_practice_timestamp')

			else:
				get_information_data=''

			return render(request,'report_practice_record.html',{
				
				"get_information_data":get_information_data,
				
				})
		else:
			 return redirect('http://'+request.get_host()+'/')
	except Exception as ex:
			log.error('Error in json data: %s' % ex)

@csrf_exempt
def openUserRecordsAnswerRecord(request,data):
	try:
		get_key = request.session.has_key('user_id')
		
		if get_key!=False:
			if UserSavedDatabaseAnswer.objects.filter(register_user_id=request.session['user_id'],id=data).exists():
				get_information_data=UserSavedDatabaseAnswer.objects.filter(register_user_id=request.session['user_id'],id=data).values('question_name','user_answers','corrected_answer')
			return render(request,'answer_record.html',{
				
				"get_information_data":get_information_data,
				
				})
		else:
			 return redirect('http://'+request.get_host()+'/')
	except Exception as ex:
			log.error('Error in json data: %s' % ex)
